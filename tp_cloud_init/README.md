# TP1 : cloud-init

## I. Premiers pas

```
vagrant@ubuntu:/home$ ls
it4  vagrant

vagrant@ubuntu:/tmp$ ls
b3_git_version
```

## II. Go further



```
vagrant@ubuntu:/home$ ls
ansible-admin  docker-admin  it4  vagrant
```

## III. Ansible again
On lance le playbook

```ansible-playbook -i hosts.ini nginx.yml --user ansible-admin```

On peut joindre le site nginx via 192.168.50.2:8080

![](https://i.imgur.com/LNTd1Xz.png)
