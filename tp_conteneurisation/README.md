# TP3 : Conteneurisation

# Docker

**Déterminez...**

- le path du dossier de données de Docker
  - l'endroit où tout ce qui est lié à Docker est stocké
  - les images, les données des conteneurs actifs, etc.

```
/var/lib/docker
```

- pourquoi est-ce qu'être membre du groupe `docker` permet de l'utiliser ?
  - il y a bien un endroit où c'est écrit !
  - n'hésitez pas à m'appeler pour ça

```
Le groupe docker donne des droits equivalents a ceux de root
```

- le path du fichier de conf de Docker

```
/etc/docker/daemon.json
```
Le fichier n'existe pas de base il faut le creer

**Analyser les processus liés au démon**

processus docker:
```
quentin@quentin-WF75-10TI:~/Bureau/vagrant/tp_cloud_b3$ ps -eF | grep docker
root        5010       1  0 493228 74392  3 14:45 ?        00:00:00 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock
```

**Analyse les processus liés à chaque conteneur**

On lance un conteneur
```
quentin@quentin-WF75-10TI:~/Bureau/vagrant/tp_cloud_b3$ docker run -d debian sleep 99999
e77d4e6e08868b40e7026190dcbcc06f06681c3ea9dfc395a3babe927a73c3fc
quentin@quentin-WF75-10TI:~/Bureau/vagrant/tp_cloud_b3$ docker ps
CONTAINER ID   IMAGE     COMMAND         CREATED          STATUS          PORTS     NAMES
e77d4e6e0886   debian    "sleep 99999"   18 seconds ago   Up 17 seconds             focused_mirzakhani
```

Processus du conteneur:
```
quentin@quentin-WF75-10TI:~/Bureau/vagrant/tp_cloud_b3$ ps -eF | grep container
root        7455       1  0 178110 8356   2 15:10 ?        00:00:00 /usr/bin/containerd-shim-runc-v2 -namespace moby -id e77d4e6e08868b40e7026190dcbcc06f06681c3ea9dfc395a3babe927a73c3fc -address /run/containerd/containerd.sock

```

## Lancement de conteneurs

**Utiliser la commande `docker run`**

Commande pour run nginx
```
$ docker run --name web -d --cpus="1.5" --memory="1g" 1g -v ~/tp_cloud_b3/tp_conteneurisation/html:/usr/share/nginx/html -v ~/tp_cloud_b3/tp_conteneurisation/default.conf:/etc/nginx/conf.d/default.conf --user="quentin" -p 8888:80 nginx 
```

## II. Images

Build de l'image:
```
docker build . -t test:toto
```

On lance le conteneur avec l'image
```
docker run -p 80:80 test:toto
```

![](https://i.imgur.com/9WlFx3k.png)
